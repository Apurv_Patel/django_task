from selenium import webdriver
from app.models import Employee
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time

class TestProjectListPage(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = webdriver.Chrome('functional_tests/chromedriver.exe')
        
    def tearDown(self):
        self.browser.close()
        
    def test_list_url(self):
        self.browser.get(self.live_server_url)
        
        # time.sleep(20)
        url = self.live_server_url + reverse('list')
        click = self.browser.find_element_by_xpath('/html/body/div/div/div/form/div[4]/div[2]/a').click()
        self.assertEqual(
            self.browser.current_url,
            url
        )

    
    
    def test_data(self):
        self.browser.get(self.live_server_url)
        firstname = self.browser.find_element_by_xpath('//*[@id="id_firstname"]').send_keys("Apurv")
        lastname = self.browser.find_element_by_xpath('//*[@id="id_lastname"]').send_keys("Patel")
        phone_no = self.browser.find_element_by_xpath('//*[@id="id_phone_no"]').send_keys("666465465")

        submit = self.browser.find_element_by_xpath('/html/body/div/div/div/form/div[4]/div[1]/button').click()
        
        find_firstname = self.browser.find_element_by_xpath('/html/body/div/div/div/table/tbody/tr[1]/td[1]').text
        find_lastname = self.browser.find_element_by_xpath('/html/body/div/div/div/table/tbody/tr[1]/td[2]').text
        find_phone_no = self.browser.find_element_by_xpath('/html/body/div/div/div/table/tbody/tr[1]/td[3]').text

        self.assertEqual(find_firstname,'Apurv')
        self.assertEqual(find_lastname,'Patel')
        self.assertEqual(find_phone_no,'666465465')

    def test_update_url(self):
        obj = Employee.objects.create(
             firstname='Anil',
             lastname='Patel',
             phone_no=88888888,
         )
        self.browser.get(self.live_server_url)
        
        url = self.live_server_url + reverse('update',kwargs={'id':obj.id})
        list_button_click = self.browser.find_element_by_xpath('/html/body/div/div/div/form/div[4]/div[2]/a').click()
        update_button_click = self.browser.find_element_by_xpath('/html/body/div/div/div/table/tbody/tr[1]/td[4]/a/i').click()
        
        self.assertEqual(self.browser.current_url,url)
  
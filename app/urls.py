from django.urls import path
from app import views
urlpatterns = [
    path('',views.employee_form,name='insert'),
    path('<int:id>/',views.employee_update,name="update"),
    path('delete/<int:id>/',views.employee_delete,name="delete"),
    path('list/',views.employee_list,name="list"),
]

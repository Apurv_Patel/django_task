from django.test import SimpleTestCase
from app.forms import EmployeeForm


class TestForms(SimpleTestCase):
    
    def test_employee_form_valid_data(self):
        # valid form with all data
        form = EmployeeForm(data={
            'firstname':'apurv',
            'lastname':'patel',
            'phone_no':454545454
        })
        
        
        self.assertTrue(form.is_valid())
    
    def test_employee_form_invalid_data(self):
        # invalid form without phone_no 
        form = EmployeeForm(data={
            'firstname':'Apurv',
            'lastname':'Patel',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors),1)
from django.test import TestCase
from app.models import Employee
# Create your tests here.


class TestModel(TestCase):
    
    def test_fields(self):
        employee = Employee()
        employee.firstname = "Anil"
        employee.lastname = "raqnput"
        employee.phone_no = 8160242110
        employee.save()
        
        record = Employee.objects.get(id=1)
        self.assertEqual(record,employee)
        
        
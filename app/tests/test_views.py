from django.test import TestCase,Client
from django.urls import reverse

from app.models import Employee
from app.forms import EmployeeForm

class TestViews(TestCase):
    
    def setUp(self):
        self.client = Client()

    def test_post(self):
        url = reverse('insert')
        response = self.client.post(url,{
            'firstname':'patel',
            'lastname':'apurv',
            'phone_no':54425454
        })
        self.assertEqual(response.status_code,302)
        employee = Employee.objects.get(id=1)
        self.assertEqual(employee.phone_no,'54425454')
    
    def test_update(self):
        obj = Employee.objects.create(
             firstname='Anil',
             lastname='Patel',
             phone_no=88888888,
         )
        
        url = reverse('update',kwargs={'id':obj.id})
        
        response = self.client.post(url,{
            'firstname':'New Data',
            'lastname':'New One',
            'phone_no':221211212
        })
        self.assertEqual(response.status_code,302)
        employee = Employee.objects.get(id=obj.id)
        self.assertEqual(employee.phone_no,'221211212')
    
    def test_list(self):
        url = reverse('list')
        response = self.client.get(url)
        
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'employee_list.html')
        
    def test_delete(self):
        employee = Employee.objects.create(
             firstname='Anil',
             lastname='Patel',
             phone_no=88888888,
         )
        url = reverse('delete',kwargs={'id': employee.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code,302)
        self.assertEqual(Employee.objects.count(),0)
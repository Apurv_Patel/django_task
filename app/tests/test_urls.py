from django.test import SimpleTestCase
from django.urls import reverse,resolve
from app.views import employee_list,employee_form,employee_update,employee_delete

class TestUrls(SimpleTestCase):
    
    def test_list(self):
        url = reverse('list')
        # match url_function_name 
        # If test for class based view then simply "resolve(url).func.view_class"
        self.assertEqual(resolve(url).func,employee_list)
    
    def test_insert(self):
        url = reverse('insert')
        self.assertEqual(resolve(url).func,employee_form)
        
    def test_delete(self):
        url = reverse('delete',args=[1])
        self.assertEqual(resolve(url).func,employee_delete)
        
    def test_update(self):
        url = reverse('update',args=[1])
        self.assertEqual(resolve(url).func,employee_update)